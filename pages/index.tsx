import Head from "next/head"
import { Canvas, useUpdate, useLoader } from "react-three-fiber"
import gsap from "gsap"
import { Object3D } from "three"
import { OrbitControls } from "@react-three/drei"
import { Texts } from "../components/Texts"
import { Layout } from "../components/Layout"
import { NearestFilter } from "three"
import { Suspense } from 'react'
import { TextureLoader } from 'three/src/loaders/TextureLoader'
import styles from '../styles/Home.module.css'
import { useState } from 'react'
import swal from 'sweetalert';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'



//logic cube


function Box() {

  const colorMap = useLoader(TextureLoader, '/textures/daimonsvg.svg');

  const mesh = useUpdate((mesh: Object3D) => {
    gsap.to(mesh.position, { duration: 1, delay: 1.5, x: 2 })
  }, [])

  return (
    <mesh ref={mesh}>
      <boxGeometry args={[1.5, 1.5, 1.5]} />
      <meshBasicMaterial map={colorMap} />
    </mesh>
  )
}

export default function Animation() {
//logic form

const MySwal = withReactContent(Swal)


const [name, setName] = useState('')
const [email, setEmail] = useState('')
const [phone, setPhone] = useState('')
const [referido, setRef] = useState('')
const [message, setMessage] = useState('')
const [submitted, setSubmitted] = useState(false)

  const handleSubmit = (e) => {

     e.preventDefault()


    if (name.length == 0 || email.length == 0 || message.length == 0){
        
     Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Please fill all items',
        heightAuto: true,
        showConfirmButton: false,
        timer: 3500
       }) 
         return false;
          /*  swal("No Send!", "Please fill all items", "error")
           return false; */
   }
     else{


     Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Thanks for contact me',
        heightAuto: true,
        showConfirmButton: false,
        timer: 3500
       }) 
 


/*
     swal("Send Success!", "Thanks for contact me", "success")
    console.log('Sending')

    function reload() {
    document.location.reload();
    }
  setTimeout(reload, 5000);
 */
   }

    let data = {
        name,
        email,
        phone,
        referido,
        message
    }
    fetch('/api/contact', {
      method: 'POST',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then((res) => {
        console.log('Response received')
        if (res.status === 200) {
            console.log('Response succeeded!')
            setSubmitted(true) 
            setName('')
            setEmail('')
            setPhone('')
            setRef('')
            setMessage('')
        }
    })
  }




  return (
    <Layout>
      <Head>
        <title>Contact Me</title>
      </Head>




      <Canvas className="bg-black">

      <Suspense fallback={null}>
   
      <Box />
      <Texts position={[-0.2, 0.2, 0.2]}>

<div className={styles.container}>
  <div className={styles.title}>CONTACT ME</div>
   <p className={styles.title}>
    (click/drag over space black)
  </p>
< form className={styles.main}>
   <div> < label htmlFor='name'>Name</label></div>
   <div> < input type='text' onChange={(e)=>{setName(e.target.value)}} name='name' className={styles.inputField}/></div>
   <div> < label htmlFor='name'>Email</label></div>
   <div> < input type='text' onChange={(e)=>{setEmail(e.target.value)}} name='name' className={styles.inputField}/></div>
   <div> < label htmlFor='name'>Message</label></div>
    < textarea onChange={(e)=>{setMessage(e.target.value)}} name='name' className={styles.inputMessage}/>
    <div>
      <button className={styles.inputButton1}  onClick={(e)=>{handleSubmit(e)}}>Send</button>
    </div>

 </form >
</div>
          


        </Texts>
        <OrbitControls />
        </Suspense>
      </Canvas>
    </Layout>
  )
}
